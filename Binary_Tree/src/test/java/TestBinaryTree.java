import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class TestBinaryTree {

    @Test
    public void firstTest()
    {
        BinaryTree tree = new BinaryTree();
        tree.addInteger(10);
        tree.addInteger(5);
        tree.addInteger(9);
        tree.addInteger(1);
        tree.addInteger(4);
        tree.addInteger(6);
        tree.addInteger(7);
        tree.addInteger(2);
        tree.addInteger(8);
        tree.addInteger(3);

        assertEquals("Preorder failed!", "10 5 1 4 2 3 9 6 7 8 ", tree.preOrder());
        assertEquals("Inorder failed!", "1 2 3 4 5 6 7 8 9 10 ", tree.inOrder());
        assertEquals("Postorder failed!", "3 2 4 1 8 7 6 9 5 10 ", tree.postOrder());
    }

    @Test
    public void secondTest()
    {
        BinaryTree tree = new BinaryTree();
        tree.addInteger(5);
        tree.addInteger(2);
        tree.addInteger(8);
        tree.addInteger(1);
        tree.addInteger(3);
        tree.addInteger(4);
        tree.addInteger(7);
        tree.addInteger(6);
        tree.addInteger(9);
        tree.addInteger(10);

        assertEquals("Preorder failed!", "5 2 1 3 4 8 7 6 9 10 ", tree.preOrder());
        assertEquals("Inorder failed!", "1 2 3 4 5 6 7 8 9 10 ", tree.inOrder());
        assertEquals("Postorder failed!", "1 4 3 2 6 7 10 9 8 5 ", tree.postOrder());
    }

    @Test
    public void thirdTest()
    {
        BinaryTree tree = new BinaryTree();
        tree.addInteger(5);
        tree.addInteger(4);
        tree.addInteger(6);
        tree.addInteger(3);
        tree.addInteger(7);
        tree.addInteger(2);
        tree.addInteger(8);
        tree.addInteger(1);
        tree.addInteger(9);
        tree.addInteger(10);

        assertEquals("Preorder failed!", "5 4 3 2 1 6 7 8 9 10 ", tree.preOrder());
        assertEquals("Inorder failed!", "1 2 3 4 5 6 7 8 9 10 ", tree.inOrder());
        assertEquals("Postorder failed!", "1 2 3 4 10 9 8 7 6 5 ", tree.postOrder());
    }
}
