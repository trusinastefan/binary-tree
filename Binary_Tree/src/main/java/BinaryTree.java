public class BinaryTree
{
    private Node topNode;

    public void addInteger(int i)
    {
        if (topNode == null)
        {
            topNode = new Node(i);
        }
        else
        {
            Node currentNode = topNode;
            while (true) {
                if (currentNode.nodeInteger == i) {
                    break;
                } else if (currentNode.nodeInteger > i && currentNode.leftNode != null) {
                    currentNode = currentNode.leftNode;
                } else if (currentNode.nodeInteger < i && currentNode.rightNode != null) {
                    currentNode = currentNode.rightNode;
                } else if (currentNode.nodeInteger > i && currentNode.leftNode == null) {
                    currentNode.leftNode = new Node(i);
                    break;
                } else if (currentNode.nodeInteger < i && currentNode.rightNode == null) {
                    currentNode.rightNode = new Node(i);
                    break;
                }
            }
        }
        return;
    }

    public String preOrder()
    {
        return recursivePreOrder(topNode);
    }

    private String recursivePreOrder(Node n)
    {
        if (n == null)
        {
            return "";
        }
        else
        {
            return "" + n.nodeInteger + " " + recursivePreOrder(n.leftNode) + recursivePreOrder(n.rightNode);
        }
    }

    public String inOrder()
    {
        return recursiveInOrder(topNode);
    }

    private String recursiveInOrder(Node n)
    {
        if (n == null)
        {
            return "";
        }
        else
        {
            return "" + recursiveInOrder(n.leftNode) + n.nodeInteger + " " + recursiveInOrder(n.rightNode);
        }
    }

    public String postOrder()
    {
        return recursivePostOrder(topNode);
    }

    private String recursivePostOrder(Node n)
    {
        if (n == null)
        {
            return "";
        }
        else
        {
            return "" + recursivePostOrder(n.leftNode) + recursivePostOrder(n.rightNode) + n.nodeInteger + " ";
        }
    }

    private class Node
    {
        private Node(int i)
        {
            nodeInteger = i;
        }
        private int nodeInteger;
        private Node leftNode;
        private Node rightNode;
    }
}
